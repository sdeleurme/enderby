//               ___     __      __  _           
//  _   ______ _/ (_)___/ /___ _/ /_(_)___  ____ 
// | | / / __ `/ / / __  / __ `/ __/ / __ \/ __ \
// | |/ / /_/ / / / /_/ / /_/ / /_/ / /_/ / / / /
// |___/\__,_/_/_/\__,_/\__,_/\__/_/\____/_/ /_/ 
//                                              

// Client/server methods.
Meteor.methods({
  sendEvent: function (e) {
    if (Meteor.isServer) {
      console.log("Event send:", e, "from", Meteor.userId());
    }

    if (Meteor.isClient) {
      console.log("Client side running!")
    }

    return "hi";
  }
});


