//               ____          __  _                 
//   _________  / / /__  _____/ /_(_)___  ____  _____
//  / ___/ __ \/ / / _ \/ ___/ __/ / __ \/ __ \/ ___/
// / /__/ /_/ / / /  __/ /__/ /_/ / /_/ / / / (__  ) 
// \___/\____/_/_/\___/\___/\__/_/\____/_/ /_/____/  
//                                                   

// Put all collections used on the server and client side here.
Templates = new Mongo.Collection("templates");
Notes     = new Mongo.Collection("notes");
Logs      = new Mongo.Collection("logs");
