//                      __           
//    _________  __  __/ /____  _____
//   / ___/ __ \/ / / / __/ _ \/ ___/
//  / /  / /_/ / /_/ / /_/  __/ /    
// /_/   \____/\__,_/\__/\___/_/     
//                                   
if (Meteor.isClient) {
  // Set this as true to avoid nav issues on reload 
  Iron.Location.configure({useHashPaths: false}); 
}

Router.onBeforeAction(function () {
  if (!Meteor.user()){
    this.render('login');
    this.next();
  }
  else { this.next(); }
});

Router.configure({
  layoutTemplate   : 'layout'  ,
  notFoundTemplate : 'notFound',
  loadingTemplate  : 'loading' 
});

Router.route('/template/:_id', function () {
  console.log("cn:", this.params._id);
  this.state.set('templateId', this.params._id);
  this.render('templates'); 
});

// Router.route('#!templates', function () {
//   this.render('templates'); 
// });

Router.route('/templates', function () {
  this.render('templates'); 
});

Router.route('/templates/:_id', function () {
  this.render('template', {
    data: function () {
      return Templates.findOne({_id: this.params._id});
    }
  });
});

Router.route('/', function () {
  if (this.ready()) this.render('main'); 
  else this.render('loading'); // TODO: does this actually work?
});
