//    ________  ______   _____  _____
//   / ___/ _ \/ ___/ | / / _ \/ ___/
//  (__  )  __/ /   | |/ /  __/ /    
// /____/\___/_/    |___/\___/_/     
//                                   
//
EventEmitter = Npm.require('events').EventEmitter;
e = new EventEmitter();

// Allows event callbacks to have access to the current user.
CurrentUserId = null;
Meteor.publish(null, function() {
  CurrentUserId = this.userId;
});

Meteor.startup(function () {

  while (Templates.find().count() < 10) {
    Templates.insert({
      name: Fake.sentence(5), 
      created: new Date(),
      contents:[
        {type:'checkbox', name:'Does this exist'} 
      ]
    });
  }

  while (Logs.find().count() < 50) {
    Logs.insert({
      name: Fake.sentence(5),
      created: new Date()
    });
  }

  var flagged = true;
  while (Notes.find().count() < 50) {
    Notes.insert({
      name: Fake.sentence(5), 
      created: new Date(),
      flagged: flagged
    });
    flagged = !flagged;
  }

});

//                  __    ___      __  
//     ____  __  __/ /_  / (_)____/ /_ 
//    / __ \/ / / / __ \/ / / ___/ __ \
//   / /_/ / /_/ / /_/ / / (__  ) / / /
//  / .___/\__,_/_.___/_/_/____/_/ /_/ 
// /_/                                 
// 
// Find the documents in a collection that match the selector.
// 
// Arguments
//   selector - Mongo Selector, Object ID, or String
//   A query describing the documents to find
// 
// Options
//   sort  {a: 1, b: -1}
//   Sort order (default: natural order)
//   
//   skip Number
//   Number of results to skip at the beginning
//   
//   limit Number
//   Maximum number of results to return
//   
//   fields Mongo Field Specifier
//   Dictionary of fields to return or exclude.
//   
//   reactive Boolean
//   (Client only) Default true; pass false to disable reactivity
//   
//   transform Function
//   Overrides transform on the  Collection for this cursor. Pass null to disable
//   transformation.
// 
Meteor.publish('templates', function(limit) {
  var dl = limit || 100;
  return Templates.find({}, {limit:dl, sort:{created:-1}}); 
});

// This is the most flexible form of query.
Meteor.publish('logs', function(selector, options) {
  selector      = selector || {};
  options       = options  || {};
  options.limit = options.limit || 50;
  return Logs.find(selector, options);
});

Meteor.publish('notes', function(flag) {
  var f = flag || false ;
  return Notes.find({flagged: f}); 
});

// The allow method accepts three possible callbacks: insert, remove, and
// update. The first argument to all three callbacks is the _id of the logged
// in user, and the remaining arguments are as follows:
//
//   insert(userId, document)
//
// document is the document that is about to be inserted into the database.
// Return true if the insert should be allowed, false otherwise.
//
//   update(userId, document, fieldNames, modifier)
//
// document is the document that is about to be modified. fieldNames is an
// array of top-level fields that are affected by this change. modifier is the
// Mongo Modifier that was passed as the second argument of collection.update.
// It can be difficult to achieve correct validation using this callback, so it
// is recommended to use methods instead. Return true if the update should be
// allowed, false otherwise.
//
//   remove(userId, document)
//
// document is the document that is about to be removed from the database.
// Return true if the document should be removed, false otherwise.
//
Notes.allow({
  insert: function (userId, note) {
    check(note.name, String);
    if (note.created || note.modified) {
      console.error("Can't explicitly set modified or created.");
      return false ;
    }
    else {
      note.created = note.modified = new Date();
      return true;
    }
  },
  update: function (userId, document, fieldNames, modifier) {
    if (_.contains(fieldNames, ["created", "updated"])) {
      console.error("Can't explicitly set modified or created.");
      return false ;
    }
    else {
      modifier.$set = modifier.$set || {};
      modifier.$set.modified = new Date();
      modifier.$set.ownerId  = userId;
      return true;
    }
  },
  remove: function (userId, note) {
    // can only delete your own notes
    return note.createdBy === userId;
  }
});

Logs.allow({
  insert: function (userId, d) {
    check(d.name, String);
    if (d.created || d.modified) {
      console.error("Can't explicitly set modified or created.");
      return false ;
    }
    else {
      d.created = d.modified = new Date();
    }
    return true
  },
  update: function (userId, document, fieldNames, modifier) {
    return false;
  },
  remove: function (userId, note) {
    return false;
  }
});

Templates.allow({
  insert: function (userId, d) {
    check(d.name, String);
    if (d.created || d.modified) {
      console.error("Can't explicitly set modified or created.");
      return false ;
    }
    else {
      d.created = d.modified = new Date();
    }
    return true
  },
  update: function (userId, d, fieldNames, modifier) {
    if (_.contains(fieldNames, ["created", "updated"])) {
      console.error("Can't explicitly set modified or created.");
      return false ;
    }
    else {
      modifier.$set = modifier.$set || {};
      modifier.$set.modified = new Date();
      modifier.$set.ownerId  = userId;
      return true;
    }
  },
  remove: function (userId, d) {
    return true;
  }
});

e.on('testevent', Meteor.bindEnvironment(function testevent(data, userId) {
  var f = Templates.findOne({name:/^tion/i});
  console.log("testevent:", data, CurrentUserId, f._id);
}));
