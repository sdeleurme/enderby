//                __                   _       __  _                 
//    _______  __/ /_  _______________(_)___  / /_(_)___  ____  _____
//   / ___/ / / / __ \/ ___/ ___/ ___/ / __ \/ __/ / __ \/ __ \/ ___/
//  (__  ) /_/ / /_/ (__  ) /__/ /  / / /_/ / /_/ / /_/ / / / (__  ) 
// /____/\__,_/_.___/____/\___/_/  /_/ .___/\__/_/\____/_/ /_/____/  
//                                  /_/                              
Meteor.subscribe("templates", 100);
Meteor.subscribe("notes");
Meteor.subscribe("logs",{}, {limit:8});

Session.set("messages", 0);
Session.set("verbose", false);

//          __                                       
//   ____  / /_  ________  ______   _____  __________
//  / __ \/ __ \/ ___/ _ \/ ___/ | / / _ \/ ___/ ___/
// / /_/ / /_/ (__  )  __/ /   | |/ /  __/ /  (__  ) 
// \____/_.___/____/\___/_/    |___/\___/_/  /____/  

// Example of an client side observer 
var handle = Templates.find().observeChanges({
  added: function (id) {
    if (Session.get("verbose")) console.log("template added:", id);
    // if (!initializing) self.changed("counts", roomId, {count: count});
  },
  removed: function (id) {
    if (Session.get("verbose")) console.log("template removed:", id);
    // self.changed("counts", roomId, {count: count});
  },
  changed: function (id, fields) {
    if (Session.get("verbose")) console.log("template changed:", fields);
  }
});


//                __                       
//   ____ ___  __/ /_____  _______  ______ 
//  / __ `/ / / / __/ __ \/ ___/ / / / __ \
// / /_/ / /_/ / /_/ /_/ / /  / /_/ / / / /
// \__,_/\__,_/\__/\____/_/   \__,_/_/ /_/ 
//                                         

// Example of a session aware autorun. 
Tracker.autorun(function (computation) {
  if (!Session.equals("forms", true)) return;
});

