//     __         __                    
//    / /_  ___  / /___  ___  __________
//   / __ \/ _ \/ / __ \/ _ \/ ___/ ___/
//  / / / /  __/ / /_/ /  __/ /  (__  ) 
// /_/ /_/\___/_/ .___/\___/_/  /____/  
//             /_/                      

// Register some template helpers that seemed, well, helpful.
_.each({
  error: function() {
    return Session.get('error');
  },
  toLowerCase: function(text) {
    return text && text.toLowerCase();
  },
  toUpperCase: function(text) {
    return text && text.toUpperCase();
  },
  firstChar: function(text) {
    return text && text[0].toUpperCase();
  },
  sessionBool: function(prop) {
    return Session.get(prop) ? 'true' : 'false';
  },
  session: function(prop) {
    return Session.get(prop);
  },
  localeDate: function(date) {
    return date.toLocaleDateString();
  },
  isThen: function(a, then) {
    return a ? then : '';
  },
  isNotThen: function(a, then) {
    return a ? '' : then;
  },
  isTrue: function(a, b, consl) {
    // Template tag # if isTrue a b consl 
    if (_.isBoolean(consl)) console.log(a, b, a==b);
    return a == b;
  },
  isTrueThen: function(a, b, then) {
    return a == b ? then : false;
  },
  isUserThen: function(a, b) {
    return this.userId == Meteor.userId() ? a : b;
  },
  isMobile: function(a) {
    return Meteor.isCordova() ? a : false;
  },
  isMobileThen: function(a, b) {
    return Meteor.isCordova() ? a : b;
  }
}, function(fn, name) { Blaze.registerHelper(name, fn); });
