Template.header.events({
  'click .verbose': function(e) {
    Session.set("verbose", !Session.get("verbose"));
  },

  'click .inbox': function(e) {
    Router.go("/");
  },

  'click .templates': function(e) {
    Router.go("/templates");
  },

  'click .logout': function(e) {
    Meteor.logout();
  },
});                                             

Template.header.helpers({
  email: function email(){
    return Meteor.user().emails[0].address;
  }
});
