Template.templates.helpers({
  templates: function () {
    return Templates.find();
  },
  settings: function () {
    return {
      collection: Templates.find(),
      rowsPerPage: 10,
      showFilter: true,
      // showColumnToggles: true, 
      fields: [
        {key:'_id'      , label:'Id', tmpl: Template.templateId},
        {key:'name'     , label:'Name'     },
        {key:'modified' , label:'Modified' },
      ]
    };
  }
});

Template.templates.events({
  "click .reactive-table tr": function (e, o) {
    e.preventDefault();
    // clicking on the id takes you to the detail page
    if (e.target.className == "seek") {
      Router.go("/templates/" + this._id);
    }
    else {
      Templates.update({ _id: this._id }, { $set: { name: Fake.sentence(4) }});
    }
  },
});

Template.template.helpers({ });
